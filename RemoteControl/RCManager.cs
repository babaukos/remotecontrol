﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Drawing.Imaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace RemoteControl
{
    public partial class RCManager : Form
    {
        private int timerTick = 100;             //  3 секунды
        private TcpClient client;
        private TcpListener server;
        public NetworkStream mainStream;

        private Thread listening;
        private Thread getImage;

        private Thread massege;
        public NetworkStream massegeStream;

        private Int32 portNumber;
        private IPAddress ipNumber;

        private RCViewer observer;
        private RCChat chat;
        private Label Consol;
        private String key;

        private void MoveCursor()
        {
            // Set the Current cursor, move the cursor's Position,
            // and set its clipping rectangle to the form. 

            this.Cursor = new Cursor(Cursor.Current.Handle);
            Cursor.Position = new Point(Cursor.Position.X - 50, Cursor.Position.Y - 50);
            Cursor.Clip = new Rectangle(this.Location, this.Size);
        }
        // Таймертик на скриншоты
        private void timer1_Tick(object sender, EventArgs e)
        {
            TranciveImage();
        }
        // Захват изображения с рабочего стола
        private static Image GrabDesktop() 
        {
            Rectangle bound = Screen.PrimaryScreen.Bounds;
            Bitmap screenshot = new Bitmap(bound.Width, bound.Height, PixelFormat.Format32bppArgb);
            Graphics graphic = Graphics.FromImage(screenshot);
            graphic.CopyFromScreen(bound.X, bound.Y, 0, 0, bound.Size, CopyPixelOperation.SourceCopy);
            return screenshot;
        }
        // Инициализация формы
        public RCManager()
        {
            InitializeComponent();
        }
        // При появлении формы
        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Interval = timerTick;
            textBox3.Text = "2015";
            textBox2.Text = "127.0.0.1";

            Consol = label5;
        }
        // При закрытии формы
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            StopLstenner();
        }
        // Посмотреть екран
        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{
                ipNumber = IPAddress.Parse(textBox2.Text);
                portNumber = Int32.Parse(textBox3.Text);

                if (button1.Text.StartsWith("Connect"))
                {
                    button1.Text = "Disconnect";
                    Connect();
                    textBox3.Enabled = false;
                    textBox2.Enabled = false;
                    textBox4.Enabled = false;
                    button2.Enabled = false;
                }
                else
                    {
                        button1.Text = "Connect";
                        Disconect();
                        textBox3.Enabled = true;
                        textBox2.Enabled = true;
                        textBox4.Enabled = true;
                        button2.Enabled = true;
                    }
            //}
            //catch (Exception)
            //{
            //  button1.Text = "Connect";
            //  if (observer!=null) observer.Close();
            //  Consol.Text = "Ошибка соединения...";
            //}

        }
        // Показать екран
        private void button2_Click(object sender, EventArgs e)
        {
            ipNumber = IPAddress.Any;
            portNumber = Int32.Parse(textBox3.Text);

            //try
            //{
                if (button2.Text.StartsWith("Show"))
                {
                    button2.Text = "Breack Show";
                    StartLstenner();
                    textBox3.Enabled = false;
                    textBox2.Enabled = false;
                    textBox4.Enabled = false;
                    button1.Enabled = false;
                }
                else
                    {
                        button2.Text = "Show";
                        StopLstenner();
                        textBox3.Enabled = true;
                        textBox2.Enabled = true;
                        textBox4.Enabled = true;
                        button1.Enabled = true;
                    }
            //}
            //catch(Exception)
            //{
            //    button2.Text = "Show";
            //    Consol.Text = "Ошибка создания сервера...";
            //}
        }
        //
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RCAbout windowAbout;
            windowAbout = new RCAbout();
            windowAbout.Show();
        }

        //----------------------Клиентские методы---------------------------------
        private void Connect()
        {
            client = new TcpClient();
            client.Connect(ipNumber, portNumber);
            Consol.Text = "Вы подключились";

            getImage = new Thread(ReciveImage);
            getImage.Start();
        }
        private void Disconect() 
        {
            client.Close();
            getImage.Abort();
            Consol.Text = "Вы отключились";

            if (mainStream!= null)mainStream.Close();
        }
        // Прием захваченого изображения
        private void ReciveImage()
        {
            BinaryFormatter binFormatter = new BinaryFormatter();
            while (client.Connected)
            {
                mainStream = client.GetStream();
                if (observer != null) observer.DrawImage((Image)binFormatter.Deserialize(mainStream));
            }
        }

        //----------------------Серверніе методы---------------------------------
        private void StartLstenner() 
        {
            Consol.Text = "Трансляция начата...";
            client = new TcpClient();
            server = new TcpListener(ipNumber, portNumber);// Установить TcpListener порт
            server.Start();                                // Начало прослушивания запросов клиентов

            listening = new Thread(Listen);
            listening.Start();
            client = server.AcceptTcpClient();         // Выполнить блокирующий вызов для приема запросов.
            Consol.Text = "К вам подключились";
            timer1.Start();
        }
        private void Listen() 
        {
            while (!client.Connected)
            {
                Consol.Text = "К вам подключились";
            }
        }
        private void StopLstenner() 
        {
            Consol.Text = "Трансляция закончена...";
            timer1.Stop();
            if (server!=null) server.Stop();
            if (client!=null) client = null;
            if (listening!=null) listening.Abort();
        }
        // Отправка захваченого изображения
        private void TranciveImage()
        {
            BinaryFormatter binFormatter = new BinaryFormatter();
            mainStream = client.GetStream();
            binFormatter.Serialize(mainStream, GrabDesktop());
        }

        //-----------------------------Чат---------------------------------------
        //Для чтения используется
        public string ReadMessege()
        {
            while (client.Connected)
            {
                //NetworkStream stream = client.GetStream();
                byte[] data = new byte[256];
                int bytes = mainStream.Read(data, 0, data.Length); // получаем количество считанных байтов
                string message = Encoding.UTF8.GetString(data, 0, bytes);
                return message;
            }
            return "";
        }
        //Для отправки данных у потока
        public void WritMessege(string response) 
        {
            while (client.Connected)
            {
                //NetworkStream stream = client.GetStream();
                byte[] data = System.Text.Encoding.UTF8.GetBytes(response);
                mainStream.Write(data, 0, data.Length);
            }
        }
        //--------------------------Инструменты----------------------------------
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (observer == null)
            {
              observer = new RCViewer();
              observer.Show();
            }
            else if (!observer.IsDisposed && observer.Visible)
                 {
                   observer.Hide();
                 }
                 else
                    {
                      observer.Show();
                    }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (chat == null)
            {
                chat = new RCChat(this);
                chat.Show();
            }
            else if (chat != null && !chat.IsDisposed && chat.Visible)
                 {
                    chat.Hide();
                 }
                 else
                     {
                       chat.Show();
                     }
        }
    }
}
