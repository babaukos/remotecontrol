﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControl
{
    public partial class RCViewer : Form
    {
        public RCViewer()
        {
            InitializeComponent();
        }
        // Отрисовка полученого изображения
        public void DrawImage(Image img) 
        {
            pictureBox1.Image = img;
        }

    }
}
