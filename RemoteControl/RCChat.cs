﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteControl
{
    public partial class RCChat : Form
    {
        private RCManager mainManager;
        public RCChat()
        {
            InitializeComponent();
        }
        public RCChat(RCManager manager)
        {
            mainManager = manager;
            InitializeComponent();
            if (mainManager.mainStream !=null)
            {
                listBox1.Text += mainManager.ReadMessege();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            mainManager.WritMessege(textBox2.Text);
            listBox1.Text += textBox2.Text;
            textBox2.Text = "";
        }

        private void RCChat_FormClosing(object sender, FormClosingEventArgs e)
        {
            Hide();
        }
    }
}
